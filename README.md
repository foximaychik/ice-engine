![Ice Engine](https://github.com/foximaychik/ice-engine/blob/b2.0/ice_engine_logo.png)
<p align="center"> My own 2D game engine, that was made on Java Swing. </p>
<br>

## Content list
- *[Downloading](https://github.com/foximaychik/ice-engine/blob/main/README.md#how-to-download-it)*
- *[Versions](https://github.com/foximaychik/ice-engine/blob/main/README.md#Versions)*

## How to download it?
- Download *[JDK](https://www.oracle.com/java/technologies/downloads/archive/#JavaSE)* (>JDK 20)
- Download *[IntelliJ IDEA](https://www.jetbrains.com/idea/download/)*
- Download .zip of Ice Engine code
<br>
That's all!

## Versions
- Version b1.0 - the engine was created!
- Version b1.1 - I added ball, new phisics and XY text
- Version b2.0 - the clearest code, new test functions, colors and etc.
